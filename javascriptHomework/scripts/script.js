function buttonClick() {
  var x1 = parseInt(document.getElementById("x1").value);
  var x2 = parseInt(document.getElementById("x2").value);

  if (
    document.getElementById("x1").value == "" ||
    document.getElementById("x2").value == ""
  ) {
    alert("Error! Field X1 and X2 must be filled");
  } else if (Number.isNaN(x1) || Number.isNaN(x2)) {
    alert("Error! Field X1 or X2 must contain only numeric values");
  }

  var resultDiv = document.getElementById("result");
  var operations = document.getElementsByName("operation");
  resultDiv.innerHTML = "";

  // Sum of elements between x1 and x2 (x1 and x2 are included)
  if (operations[0].checked) {
    var sum = 0;

    for (var i = x1; i <= x2; i++) {
      sum += i;
    }

    resultDiv.append(
      "Sum of numbers between " + x1 + " and " + x2 + " is: " + sum
    );
  }
  // Multiplication of elements between x1 and x2 (x1 and x2 are included)
  else if (operations[1].checked) {
    var multi = 1;

    for (var i = x1; i <= x2; i++) {
      multi *= i;
    }

    resultDiv.append(
      "Multi of numbers between " + x1 + " and " + x2 + " is: " + multi
    );
  }
  // Search of all prime numbers between x1 and x2 (x1 and x2 are included)
  else if (operations[2].checked) {
    var isPrime = false;

    resultDiv.append("Prime numbers between " + x1 + " and " + x2 + " are: ");
    for (var i = x1; i <= x2; i++) {
      isPrime = true;
      for (var j = 2; j < i; j++) {
        if (i % j == 0) {
          isPrime = false;
        }
      }

      if (isPrime) {
        resultDiv.append(+i + ", ");
      }
    }
  }
}

function cleanFields() {
  document.getElementById("x1").value = "";
  document.getElementById("x2").value = "";
  document.getElementById("result").innerHTML = "";
}
